// Soal nomor 1.
console.log("If Else");
console.log("                         ");

var nama = "John";
var peran = "";
//Jika peran dan nama kosong, maka harus diisi
//Jika nama terisi dan peran kosong, maka statement 2 keluar
if (nama == "", peran == ""){
    console.log ("Nama dan Peran Harus Diisi!")
} else {(nama = "John")
    console.log ("Hai John, Pilih peranmu untuk memulai game Werewolf!")
}
console.log("                         ");
console.log("-------------------------");

var nama2 = "Jane";
var nama3 = "Jenita";
var nama4 = "Juan";
var peran1= "Penyihir";
var peran2= "Guard";
var peran3= "Werewolf";

if (nama2 == "Jane", peran == ""){
    console.log ("Hai " + nama2 +" "+ "Pilih peranmu untuk memulai game Werewolf!")
} else (peran == "Penyihir")
    console.log("Selamat datang di Dunia Werewolf, " + nama2);
    console.log("Halo " + peran1 + " " + nama2 + ", kamu dapat melihat siapa yang menjadi werewolf");

console.log("                         ");
console.log("-------------------------");

if (nama3 == "Jenita", peran == ""){
    console.log ("Hai " + nama3 +" "+ "Pilih peranmu untuk memulai game Werewolf!")
} else (peran == "Guard") 
    console.log("Selamat datang di Dunia Werewolf, " + nama3);
    console.log("Halo " + peran2 + " " + nama3 + ", kamu akan membantu melindungi temanmu dari serangan werewolf");

console.log("                         ");
console.log("-------------------------");

if (nama4 == "Juan", peran == ""){
    console.log ("Hai " + nama4 +" "+ "Pilih peranmu untuk memulai game Werewolf!")  
} else (peran == "Werewolf")
    console.log("Selamat datang di Dunia Werewolf, " + nama4);
    console.log("Halo " + peran3 + " " + nama4 + ", kamu akan memakan mangsa setiap hari agar tetap menjadi werewolf");

console.log("                         ");
console.log("-------------------------");
console.log("-------------------------");

// Soal nomor 2.
console.log("Switch Case");

var tanggal = 1
var bulan = 5
var tahun = 1945

switch (tanggal) {
    case 1:
        console.log("Sekarang Tanggal 1")
        break;
    default:
        console.log("Tidak ditemukan")
        break;
}

switch (bulan) {
    case 1:
        console.log("Januari")
        break;
    case 2:
        console.log("Februari")
        break;
    case 3:
        console.log("Maret")
        break;
    case 4:
        console.log("April")
        break;
    case 5:
        console.log("Mei")
        break;
    case 6:
        console.log("Juni")
        break;
    case 7:
        console.log("Juli")
        break;
    case 8:
        console.log("Agustus")
        break;
    case 9:
        console.log("September")
        break;
    case 10:
        console.log("Oktober")
        break;
    case 11:
        console.log("November")
        break;
    case 12:
        console.log("Desember")
        break;

    default:
        console.log("Tidak ditemukan")
        break;
}

switch (tahun) {
    case 1945:
        console.log("1945")
        break;
    default:
        console.log("Tidak ditemukan")
        break;
}