// Soal nomor 1.
console.log("Soal nomor 1");
var word = "JavaScript";
var second = "is";
var third = "awesome";
var fourth = "and";
var fifth = "I";
var sixth = "love";
var seventh = "it!"
console.log(word, second, third, fourth, fifth, sixth, seventh);
console.log("                         ");
console.log("-------------------------");

// Soal nomor 2
console.log("Soal nomor 2");
var sentence = "I am going to be React Native Developer";
var exampleFirstWord = sentence [0];
var exampleSecondWord = sentence [2] + sentence [3];
var exampleThirdWord = sentence [4] + sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var exampleFourthWord = sentence [11] + sentence[12] ;
var exampleFifthWord = sentence [14] + sentence[15];
var exampleSixthWord = sentence[17] + sentence [18] + sentence[19]+ sentence[20]+ sentence[21]+ sentence[22];
var exampleSeventhWord = sentence [23] + sentence[24]+ sentence[25]+ sentence[26]+ sentence[27]+ sentence[28];
var exampleEigthWord = sentence[30] + sentence[31]+ sentence[32]+ sentence[33]+ sentence[34]+ sentence[35]+ sentence[36]+ sentence[37]+ sentence[38];

console.log('First Word: ' + exampleFirstWord);
console.log('Second Word: ' + exampleSecondWord);
console.log('Third Word: ' + exampleThirdWord);
console.log('Fourth Word: ' + exampleFourthWord);
console.log('Fifth Word: ' + exampleFifthWord);
console.log('Sixth Word: ' + exampleSixthWord);
console.log('Seventh Word: ' + exampleSeventhWord);
console.log('Eigth Word: ' + exampleEigthWord);
console.log("                         ");
console.log("-------------------------");

// Soal nomor 3
console.log("Soal nomor 3");
var sentence2 = "WoW JavaScript is so cool";

var exampleFirstWord2 = sentence2.substring(0,3);
var exampleSecondWord2 = sentence2.substring(4,14);
var exampleThirdWord2 = sentence2.substring(15,17);
var exampleFourthWord2 = sentence2.substring(18,20);
var exampleFifthWord2 = sentence2.substring(21,25);

console.log('First Word: ' + exampleFirstWord2);
console.log('Second Word: ' + exampleSecondWord2);
console.log('Third Word: ' + exampleThirdWord2);
console.log('Fourth Word: ' + exampleFourthWord2);
console.log('Fifth Word: ' + exampleFifthWord2);
console.log("                         ");
console.log("-------------------------");

// Soal nomor 4
console.log("Soal nomor 4");
var sentence3 = "WoW JavaScript is so cool";

var exampleFirstWord3 = sentence3.substring(0,3);
var exampleSecondWord3 = sentence3.substring(4,14);
var exampleThirdWord3 = sentence3.substring(15,17);
var exampleFourthWord3 = sentence3.substring(18,20);
var exampleFifthWord3 = sentence3.substring(21,25);

var firstWordLength = exampleFirstWord3.length
var secondWordLength = exampleSecondWord3.length
var thirdWordLength = exampleThirdWord3.length
var fourthWordLength = exampleFourthWord3.length
var fifthWordLength = exampleFifthWord3.length

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength);
console.log('Second Word: ' + exampleSecondWord3 + ', with length: ' + secondWordLength);
console.log('Third Word: ' + exampleThirdWord3 + ', with length: ' + thirdWordLength);
console.log('Fourth Word: ' + exampleFourthWord3 + ', with length: ' + fourthWordLength);
console.log('Fifth Word: ' + exampleFifthWord3 + ', with length: ' + fifthWordLength);

console.log("                         ");
console.log("---------SELESAI---------");
console.log("-------------------------");